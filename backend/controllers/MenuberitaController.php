<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use yii\web\Controller;
use yii\db\Query;

use backend\models\Berita;

class MenuberitaController extends \yii\web\Controller
{
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $berita = Berita::find()->orderBy(['idberita' => SORT_ASC])->all();
        return $this->render('index', ['beritas'=>$berita]);
    }

    public function actionView($id)
    {
        $berita = Berita::find()->where(['idberita'=>$id])->one();
        return $this->render('view', ['model' => $berita]);
    }

    public function actionCreate()
    {
        $model = new Berita();

        if ($model->load(Yii::$app->request->post())) {
        	$model->tanggal=date('Y-m-d');
        	$model->save();
            return $this->redirect(['index', 'id' => $model->idberita]);
        }
        else{
            return $this->render('create', ['model' => $model]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
        	$model->tanggal=date('Y-m-d');
        	$model->save();
            return $this->redirect(['view', 'id' => $model->idberita]);
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

     public function actionDelete($id)
    {
    	$this->findModel($id)->delete();
    	return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Berita::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
