<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use yii\web\Controller;
use yii\db\Query;

use backend\models\Agenda;

class MenuagendaController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $agenda = Agenda::find()->orderBy(['idagenda' => SORT_ASC])->all();
        return $this->render('index', ['agendas'=>$agenda]);
    }

    public function actionView($id)
    {
        $agenda = Agenda::find()->where(['idagenda'=>$id])->one();
        return $this->render('view', ['model' => $agenda]);
    }

    public function actionCreate()
    {
        $model = new Agenda();

        if ($model->load(Yii::$app->request->post())) {
        	$model->tanggal=date('Y-m-d');
        	$model->save();
            return $this->redirect(['index', 'id' => $model->idagenda]);
        }
        else{
            return $this->render('create', ['model' => $model]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
        	$model->tanggal=date('Y-m-d');
        	$model->save();
            return $this->redirect(['view', 'id' => $model->idagenda]);
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

     public function actionDelete($id)
    {
    	$this->findModel($id)->delete();
    	return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Agenda::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
