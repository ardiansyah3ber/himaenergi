<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use yii\web\Controller;
use yii\db\Query;

use backend\models\Profil;

class MenuprofilController extends \yii\web\Controller
{
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $profil = Profil::find()->orderBy(['idprofil' => SORT_ASC])->all();
        return $this->render('index', ['profils'=>$profil]);
    }

    public function actionCreate()
    {
        $model = new Profil();

        if ($model->load(Yii::$app->request->post())) {
        	$model->tanggal=date('Y-m-d');
        	$model->save();
            return $this->redirect(['index', 'id' => $model->idprofil]);
        }
        else{
            return $this->render('create', ['model' => $model]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
        	$model->tanggal=date('Y-m-d');
        	$model->save();
            return $this->redirect(['index', 'id' => $model->idprofil]);
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

    public function actionDelete($id)
    {
    	$this->findModel($id)->delete();
    	return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Profil::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
