<?php
use yii\helpers\Html;
use yii\grid\Gridview;
use yii\widgets\ActiveForm;

$this->title = 'Menu Profil';
?>

<div class="row">
	<h3 >Daftar Profil</h3>
	<hr>
	<p>
        <?= Html::a('Create Profil', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
		<table class="table table-striped">
		<thead>
			<tr>
				<th>No</th>
				<th>Judul</th>
				<th>Konten</th>
				<th>Tanggal</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php $n=0; foreach ($profils as $key) { $n++;?>
				<tr>
					<td><?php echo $n;?></td>
					<td><?php echo Html::encode($key->judul);?></td>
					<td><?php echo Html::encode($key->konten);?></td>
					<td><?php echo Html::encode($key->tanggal);?></td>
					<td>
						<?php echo Html::a(
							'<i class="glyphicon glyphicon-pencil"></i> Update',
							['update','id'=>$key->idprofil],
							['class'=>'btn btn-primary']
							);
						?>

						<?php echo Html::a(
							'<i class="glyphicon glyphicon-remove"></i> Delete',
							['delete','id'=>$key->idprofil],
							[
				            'class' => 'btn btn-sm btn-danger',
				            'data' => [
				                'confirm' => 'Are you sure you want to delete this item?',
				                'method' => 'post',
			                ]
			            ]);?>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>