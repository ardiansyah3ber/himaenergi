<?php
use yii\helpers\Html;

$this->title = 'Update Profil: ' . $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Daftar Profil', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="profil-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
