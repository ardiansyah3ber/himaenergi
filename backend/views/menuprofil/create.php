<?php
use yii\helpers\Html;

$this->title = 'Create Profil';
$this->params['breadcrumbs'][] = ['label' => 'Daftar Profil', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profil-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
