<?php
use yii\helpers\Html;

$this->title = 'Create Agenda';
$this->params['breadcrumbs'][] = ['label' => 'Daftar Agenda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agenda-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
