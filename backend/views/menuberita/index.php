<?php
use yii\helpers\Html;
use yii\grid\Gridview;
use yii\widgets\ActiveForm;

$this->title = 'Menu Berita';
?>

<div class="row">
	<h3 >Daftar Berita</h3>
	<hr>
	<p>
        <?= Html::a('Create Berita', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
		<table class="table table-striped">
		<thead>
			<tr>
				<th>No</th>
				<th>Judul</th>
				<th>Author</th>
				<th>Tanggal</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php $n=0; foreach ($beritas as $key) { $n++;?>
				<tr>
					<td><?php echo $n;?></td>
					<td><?php echo Html::encode($key->judul);?></td>
					<td><?php echo Html::encode($key->author);?></td>
					<td><?php echo Html::encode($key->tanggal);?></td>
					<td>
						<?php echo Html::a(
							'<i class="glyphicon glyphicon-search"></i> Detail',
							['view','id'=>$key->idberita],
							['class'=>'btn btn-sm btn-info']
							);
						?>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>