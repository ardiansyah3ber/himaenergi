<?php
use yii\helpers\Html;

$this->title = 'Update Berita: ' . $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Daftar Berita', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->judul, 'url' => ['view', 'id' => $model->idberita]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="berita-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
