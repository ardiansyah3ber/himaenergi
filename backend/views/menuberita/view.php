<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Daftar Berita', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="berita-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?= Html::a('Update', ['update', 'id' => $model->idberita], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idberita], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'judul',
            'author',
            'tanggal',
            'konten',
        ],
    ]) ?>

</div>