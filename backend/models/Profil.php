<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "profil".
 *
 * @property integer $idprofil
 * @property string $judul
 * @property string $konten
 * @property string $tanggal
 */
class Profil extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'konten', 'tanggal'], 'required'],
            [['konten'], 'string'],
            [['tanggal'], 'safe'],
            [['judul'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idprofil' => 'Idprofil',
            'judul' => 'Judul',
            'konten' => 'Konten',
            'tanggal' => 'Tanggal',
        ];
    }
}
