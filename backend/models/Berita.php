<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "berita".
 *
 * @property integer $idberita
 * @property string $judul
 * @property string $author
 * @property string $tanggal
 * @property string $konten
 * @property string $gambar
 */
class Berita extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'berita';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'author', 'tanggal', 'konten'], 'required'],
            [['tanggal'], 'safe'],
            [['konten'], 'string'],
            [['judul'], 'string', 'max' => 100],
            [['author'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idberita' => 'Idberita',
            'judul' => 'Judul',
            'author' => 'Author',
            'tanggal' => 'Tanggal',
            'konten' => 'Konten',
        ];
    }
}
