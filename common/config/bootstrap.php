<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

Yii::setAlias('@depanfile', dirname(dirname(__DIR__)) . '/depan');
Yii::setAlias('@belakangfile', dirname(dirname(__DIR__)) . '/belakang');

Yii::setAlias('@depan', 'http://website.himaenergi.cos/');
Yii::setAlias('@belakang', 'http://admin.himaenergi.cos/');