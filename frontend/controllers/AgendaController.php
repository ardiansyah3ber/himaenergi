<?php

namespace frontend\controllers;

class AgendaController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionDetails()
    {
    	return $this->render('details');
    }

}
