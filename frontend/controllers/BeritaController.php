<?php

namespace frontend\controllers;

use Yii;

class BeritaController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionDetails()
    {
    	return $this->render('details');
    }

}
