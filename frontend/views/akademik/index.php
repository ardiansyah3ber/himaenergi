<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Akademik | HIMA ENERGI PENS';
?>
<div class="divider"></div>

<style type="text/css">
    #gambar-kiri{
        float: left;
        margin: 20px;
        padding-right: 20px; 
    }
    #gambar-kanan{
        float: right;
        margin: 20px;
        padding-left: 20px; 
    }
</style>

<div class="content">
    <div class="container">
        <div class="posts">
            <h1>Akademik</h1>
            <img id="gambar-kiri" src="<?php echo Yii::getAlias('@depan/tema/images/logo.png');?>">
            <p>
                Sejarah Hima Energi Himpunan Mahasiswa Sistem Pembangkit Energi yang disebut menjadi HIMA ENERGI adalah suatu organisasi kemahasiswaan yang berdiri di Politeknik Elektronika Negeri Surabaya.Program Study Sistem Pembangkit Energi didirakan pada tahun 2011, mahasiswa SPE angkatan pertama menjadi mahasiswa baru dengan Prodi yang baru juga dalam lingkungan kampus PENS dan tentunya lingkup KM PENS. Hal ini menimbulkan sebuah pertanyaan baru bagaimana dengan proses pengembangan mahasiswa baru ini, dikarenakan dari segi keprofesian Himpunan Mahasiswa Teknik Elektro Industri (Hima Elin) paling mendekati dengan Sistem Pembangkit Energi maka diputuskan bahwa mahasiswa SPE dinaungi oleh Hima Elin.
            </p>
            <br>
            <p>
                Bertepatan pada tahun 2012 diadakan pergantian sistem didalam manajemen kampus dengan munculnya departemen-departemen yang menaungi Prodi. Dengan pertimbangan kesesuaian secara materi pendidikan maka diputuskan  Prodi SPE dan  Prodi Teknik Mekatronika berada dalam satu naungan departemen, sehingga pengembangan kemahasiswaan SPE dialihkan ke Himpunan Mahasiswa Teknik Mekatronika (Hima Meka).
            </p>
            <br>
            <img id="gambar-kanan" src="<?php echo Yii::getAlias('@depan/tema/images/logo.png');?>">
            <p>
                Seiring berjalannya waktu, mahasiswa SPE mulai berkembang dan mengetahui akan ranah pengembangan yang lebih sesuai dengan keprofesiannya. Maka dari itu mahasiswa SPE mengusulkan untuk dilakukan  pembentukan Hima Energi. Namun Karena dari pihak Hima Meka merasa bahwa kesiapan dari mahasiswa SPE untuk membentuk sebuah himpunan masih kurang maka dari pihak BEM melalui Kementrian Dalam Negeri melakukan mediasi diantara kedua belah pihak. Pada akhirnya disepakati untuk membentuk Badan Semi Otonom yang lebih dikenal dengan BSO SPE PENS yang berfungsi untuk mewadahi mahasiswa SPE dalam mengembangkan minat dan bakatnya dalam berorganisasi dan untuk mempersiapkan segala kebutuhan untuk mendirikan sebuah Himpunan.
            </p>
            <br>
            <p>
                Pada akhirnya, dengan berdirinya BSO SPE PENS mahasiswa SPE lebih aktif dalam melakukan propaganda dan menghimpun dukungan dari semua elemen KM untuk mendirikan sebuah himpunan. Dan akhirnya setelah seluruh kelengkapan sebuah Himpunan terpenuhi dan pengakuan dari seluruh elemen KM PENS telah didapatkan,  tepat pada tanggal 29 Desember 2014 berdirilah secara resmi Himpunan Mahasiswa Sistem Pembangkit Energi sesuai dengan SK Direktur No.4297.01/PI.14/KM/2014.
            </p>
        </div>  
    </div>
</div>

<div class="container">
    <a href="#fancy" class="info-request">
        <span class="holder">
            <span class="title">Kritik dan saran</span>
            <span class="text">Berikan kritik maupun saran anda untuk hima energi melalui website ini!</span>
        </span>
        <span class="arrow"></span>
    </a>
</div>