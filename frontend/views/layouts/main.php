<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" type="text/css" href="<?php echo Yii::getAlias('@depan');?>/tema/images/logo.png">
    <meta name="author" content="ardi-PC">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <header id="header">
        <div class="container">
            <a href="<?php echo Yii::$app->homeUrl;?>" id="logo" title="Himpunan Mahasiswa Sistem Pembangkit Energi">Himpunan Mahasiswa Sistem Pembangkit Energi</a>
            <div class="menu-trigger"></div>
            <nav id="menu">
                <ul>
                    <li><?php echo Html::a('Profil', ['site/profil']);?></li>
                    <li><?php echo Html::a('Berita', ['berita/index']);?></li>
                    <li><?php echo Html::a('Agenda', ['agenda/index']);?></li>
                </ul>
                <ul>
                    <li><?php echo Html::a('Akademik', ['akademik/index']);?></li>
                    <li><?php echo Html::a('Galeri', ['galeri/index']);?></li>
                    <li><?php echo Html::a('Kontak', ['site/kontak']);?></li>
                </ul>
            </nav>
            <!-- / navigation -->
        </div>
        <!-- / container -->
    
    </header>
    <!-- / header -->
    
    <?php echo $content;?>

    <footer id="footer">
        <div class="container">
            <section>
                <article class="col-1">
                    <h3>Alamat</h3>
                    <ul>
                        <li class="address"><a href="#">Gedung Energi Lt. 2 (EN-201) <br>Kampus Politeknik Elektronika <br>Negeri Surabaya<br>Jl. Raya ITS - Kampus PENS Sukolilo, Surabaya 60111,<br> INDONESIA</a></li>
                        <li class="mail"><a href="#">himaenergi@gmail.com</a></li>
                        <li class="phone last"><a href="#">ComingSoon</a></li>
                    </ul>
                </article>
                <article class="col-2">
                    <h3>Navigation</h3>
                    <ul>
                        <li><?php echo Html::a('Profil', ['site/profil']);?></li>
                        <li><?php echo Html::a('Berita', ['berita/index']);?></li>
                        <li><?php echo Html::a('Agenda', ['agenda/index']);?></li>
                        <li><?php echo Html::a('Akademik', ['akademik/index']);?></li>
                        <li><?php echo Html::a('Galeri', ['galeri/index']);?></li>
                        <li><?php echo Html::a('Kontak', ['site/kontak']);?></li>
                    </ul>
                </article>
                <article class="col-3">
                    <h3>Media Sosial</h3>
                    <p>Dapatkan informasi seputar Hima Energi PENS melalui Media Sosial kami</p>
                    <ul>
                        <li class="facebook"><a target="_blank" href="https://www.facebook.com/HimaEnergi/">Facebook</a></li>
                        <li class="google-plus"><a target="_blank" href="https://line.com/spy0018L">@SPY0018L</a></li>
                        <li class="twitter"><a target="_blank" href="https://twitter.com/HimaEnergi">Twitter</a></li>
                        <li class="pinterest"><a target="_blank" href="https://instagram.com/himaenergipens">Instagram</a></li>
                    </ul>
                </article>
                <article class="col-4">
                    <h3>Newsletter</h3>
                    <p>Ingin mendapatkan berita secara realtime, berlangganan aja di website kami.</p>
                    <form action="#">
                        <input placeholder="Email address..." type="email">
                        <button type="submit">Subscribe</button>
                    </form>
                </article>
            </section>

            <p class="copy">Copyright 2017 Himpunan Mahasiswa Sistem Pembangkit Energi PENS. Designed by <a href="http://www.vandelaydesign.com/" title="Designed by Vandelay Design" target="_blank">Vandelay Design</a>. All rights reserved.</p>
        </div>
        <!-- / container -->
    </footer>
    <!-- / footer -->

    <div id="fancy">
        <h2>Kritik dan Saran</h2>
        <form action="#">
            <div class="left">
                <fieldset class="mail"><input placeholder="Alamat Email" type="text"></fieldset>
                <fieldset class="name"><input placeholder="Nama" type="text"></fieldset>
                <fieldset class="subject"><input placeholder="NRP" type="text"></fieldset>
            </div>
            <div class="right">
                <fieldset class="question"><textarea placeholder="Tuliskan kritik dan/atau saran anda disini"></textarea></fieldset>
            </div>
            <div class="btn-holder">
                <button class="btn" type="submit">Kirim</button>
            </div>
        </form>
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
