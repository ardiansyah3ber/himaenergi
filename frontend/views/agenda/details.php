<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Details | HIMA ENERGI PENS';
?>
<div class="divider"></div>
<style type="text/css">
    img {
        border: 1px solid #ddd;
        border-radius: 4px;
        padding: 5px;
        width: 150px;
    }

    img:hover {
        box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
    }

    .img-responsif{
        width: 100%;
        height: auto;
    }
</style>
<div class="content">
    <div class="container">

        <div class="main-content">
            <h1>Baca Agenda</h1>
            <section class="posts-con">
                <article>
                    <div class="current-date">
                        <p>April</p>
                        <p class="date">25</p>
                    </div>
                    <div class="info">
                        <h3><?php echo Html::a('Rapat Kerja Tahunan', ['berita/details']);?></h3>
                        <p class="info-line"><span class="time">10:30 AM</span><span class="place">HH204</span></p>
                        <img class="img-responsif" src="http://crispme.com/wp-content/uploads/2015/07/Mac-OS-X-Yosemite-Wallpapers-51.jpg">
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                </article>

                <h3>Komentar</h3>
                <!-- komentar DISQUS -->
                
            </section>
        </div>
        
        <aside id="sidebar">
            <div class="widget clearfix calendar">
                <div class="head">
                    <a class="prev" href="#"></a>
                    <a class="next" href="#"></a>
                    <h4><?php echo date('F'). ' '. date('Y');?></h4>
                </div>
                <div class="table">
                    <table>
                        <tr>
                            <th class="col-1">Sen</th>
                            <th class="col-2">Sel</th>
                            <th class="col-3">Rab</th>
                            <th class="col-4">Kam</th>
                            <th class="col-5">Jum</th>
                            <th class="col-6">Sab</th>
                            <th class="col-7">Min</th>
                        </tr>
                        <tr>
                            <td class="col-1 disable"><div>26</div></td>
                            <td class="col-2 disable"><div>27</div></td>
                            <td class="col-3 disable"><div>28</div></td>
                            <td class="col-4 disable"><div>29</div></td>
                            <td class="col-5 disable"><div>30</div></td>
                            <td class="col-6"><div>1</div></td>
                            <td class="col-7"><div>2</div></td>
                        </tr>
                        <tr>
                            <td class="col-1"><div>3</div></td>
                            <td class="col-2"><div>4</div></td>
                            <td class="col-3 archival"><div>5</div></td>
                            <td class="col-4"><div>6</div></td>
                            <td class="col-5"><div>7</div></td>
                            <td class="col-6"><div>8</div></td>
                            <td class="col-7"><div>9</div></td>
                        </tr>
                        <tr>
                            <td class="col-1"><div>10</div></td>
                            <td class="col-2 upcoming"><div><div class="tooltip"><div class="holder">
                                <h4>Omnis iste natus error sit voluptatem dolor</h4>
                                <p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
                            </div></div>11</div></td>
                            <td class="col-3"><div>12</div></td>
                            <td class="col-4 upcoming"><div><div class="tooltip"><div class="holder">
                                <h4>Omnis iste natus error sit voluptatem dolor</h4>
                                <p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
                            </div></div>12</div></td>
                            <td class="col-5"><div>13</div></td>
                            <td class="col-6"><div>14</div></td>
                            <td class="col-7"><div>15</div></td>
                        </tr>
                        <tr>
                            <td class="col-1"><div>16</div></td>
                            <td class="col-2"><div>17</div></td>
                            <td class="col-3"><div>18</div></td>
                            <td class="col-4"><div>19</div></td>
                            <td class="col-5 upcoming"><div><div class="tooltip"><div class="holder">
                                <h4>Omnis iste natus error sit voluptatem dolor</h4>
                                <p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
                            </div></div>20</div></td>
                            <td class="col-6"><div>21</div></td>
                            <td class="col-7"><div>22</div></td>
                        </tr>
                        <tr>
                            <td class="col-1"><div>23</div></td>
                            <td class="col-2 upcoming"><div><div class="tooltip"><div class="holder">
                                <h4>Omnis iste natus error sit voluptatem dolor</h4>
                                <p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
                            </div></div>24</div></td>
                            <td class="col-3"><div>25</div></td>
                            <td class="col-4"><div>26</div></td>
                            <td class="col-5"><div>27</div></td>
                            <td class="col-6"><div>28</div></td>
                            <td class="col-7"><div>29</div></td>
                        </tr>
                        <tr>
                            <td class="col-1"><div>30</div></td>
                            <td class="col-2 upcoming"><div><div class="tooltip"><div class="holder">
                                <h4>Omnis iste natus error sit voluptatem dolor</h4>
                                <p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
                            </div></div>31</div></td>
                            <td class="col-3"><div>1</div></td>
                            <td class="col-4 disable"><div>2</div></td>
                            <td class="col-5 disable"><div>3</div></td>
                            <td class="col-6 disable"><div>4</div></td>
                            <td class="col-7 disable"><div>5</div></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="widget list">
                <h2>Photo gallery</h2>
                <ul>
                    <li><a href="#"><img src="<?php echo Yii::getAlias('@depan');?>/tema/images/4.png" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo Yii::getAlias('@depan');?>/tema/images/4_2.png" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo Yii::getAlias('@depan');?>/tema/images/4_3.png" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo Yii::getAlias('@depan');?>/tema/images/4_4.png" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo Yii::getAlias('@depan');?>/tema/images/4_5.png" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo Yii::getAlias('@depan');?>/tema/images/4_6.png" alt=""></a></li>
                </ul>
                <div class="btn-holder">
                    <?php echo Html::a('Show more photos', ['galeri/index'], ['class'=>'btn blue']);?>
                </div>
            </div>
        </aside>
        <!-- / sidebar -->

    </div>
    <!-- / container -->
</div>

<div class="container">
    <a href="#fancy" class="info-request">
        <span class="holder">
            <span class="title">Kritik dan saran</span>
            <span class="text">Berikan krtik maupun saran anda untuk hima energi melalui website ini!</span>
        </span>
        <span class="arrow"></span>
    </a>
</div>