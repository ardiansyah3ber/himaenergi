<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Kontak | HIMA ENERGI PENS';
?>
<div class="divider"></div>

<div class="content">
    <div class="container">
        <div class="main-content">
            <div class="posts">
                <h1>Kontak Kami</h1>

                <h3><strong>Alamat</strong></h3>
                <p>
                    Gedung Energi Lt. 2 (EN-201) <br>
                    Kampus Politeknik Elektronika Negeri Surabaya <br>
                    Jl. Raya ITS - Kampus PENS Sukolilo, Surabaya 60111
                    INDONESIA
                </p>
                <hr>

                <h3><strong>Official Account</strong></h3>
                <p>
                    Facebook : HimaEnergi<br>
                    Line : @SPY0018L<br>
                    Twitter: HimaEnergi<br>
                    Instagram : himaenergipens         
                </p>
            </div>    
        </div>

        <aside id="sidebar">
            <div class="widget clearfix calendar">
                <div class="head">
                    <a class="prev" href="#"></a>
                    <a class="next" href="#"></a>
                    <h4><?php echo date('F'). ' ' . date('Y');?></h4>
                </div>
                <div class="table">
                    <table>
                        <tr>
                            <th class="col-1">Sen</th>
                            <th class="col-2">Sel</th>
                            <th class="col-3">Rab</th>
                            <th class="col-4">Kam</th>
                            <th class="col-5">Jum</th>
                            <th class="col-6">Sab</th>
                            <th class="col-7">Min</th>
                        </tr>
                        <tr>
                            <td class="col-1 disable"><div>26</div></td>
                            <td class="col-2 disable"><div>27</div></td>
                            <td class="col-3 disable"><div>28</div></td>
                            <td class="col-4 disable"><div>29</div></td>
                            <td class="col-5 disable"><div>30</div></td>
                            <td class="col-6"><div>1</div></td>
                            <td class="col-7"><div>2</div></td>
                        </tr>
                        <tr>
                            <td class="col-1"><div>3</div></td>
                            <td class="col-2"><div>4</div></td>
                            <td class="col-3 archival"><div>5</div></td>
                            <td class="col-4"><div>6</div></td>
                            <td class="col-5"><div>7</div></td>
                            <td class="col-6"><div>8</div></td>
                            <td class="col-7"><div>9</div></td>
                        </tr>
                        <tr>
                            <td class="col-1"><div>10</div></td>
                            <td class="col-2 upcoming"><div><div class="tooltip"><div class="holder">
                                <h4>Omnis iste natus error sit voluptatem dolor</h4>
                                <p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
                            </div></div>11</div></td>
                            <td class="col-3"><div>12</div></td>
                            <td class="col-4 upcoming"><div><div class="tooltip"><div class="holder">
                                <h4>Omnis iste natus error sit voluptatem dolor</h4>
                                <p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
                            </div></div>12</div></td>
                            <td class="col-5"><div>13</div></td>
                            <td class="col-6"><div>14</div></td>
                            <td class="col-7"><div>15</div></td>
                        </tr>
                        <tr>
                            <td class="col-1"><div>16</div></td>
                            <td class="col-2"><div>17</div></td>
                            <td class="col-3"><div>18</div></td>
                            <td class="col-4"><div>19</div></td>
                            <td class="col-5 upcoming"><div><div class="tooltip"><div class="holder">
                                <h4>Omnis iste natus error sit voluptatem dolor</h4>
                                <p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
                            </div></div>20</div></td>
                            <td class="col-6"><div>21</div></td>
                            <td class="col-7"><div>22</div></td>
                        </tr>
                        <tr>
                            <td class="col-1"><div>23</div></td>
                            <td class="col-2 upcoming"><div><div class="tooltip"><div class="holder">
                                <h4>Omnis iste natus error sit voluptatem dolor</h4>
                                <p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
                            </div></div>24</div></td>
                            <td class="col-3"><div>25</div></td>
                            <td class="col-4"><div>26</div></td>
                            <td class="col-5"><div>27</div></td>
                            <td class="col-6"><div>28</div></td>
                            <td class="col-7"><div>29</div></td>
                        </tr>
                        <tr>
                            <td class="col-1"><div>30</div></td>
                            <td class="col-2 upcoming"><div><div class="tooltip"><div class="holder">
                                <h4>Omnis iste natus error sit voluptatem dolor</h4>
                                <p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
                            </div></div>31</div></td>
                            <td class="col-3"><div>1</div></td>
                            <td class="col-4 disable"><div>2</div></td>
                            <td class="col-5 disable"><div>3</div></td>
                            <td class="col-6 disable"><div>4</div></td>
                            <td class="col-7 disable"><div>5</div></td>
                        </tr>
                    </table>
                </div>
            </div>
        </aside>

    </div>
</div>

<div class="container">
    <a href="#fancy" class="info-request">
        <span class="holder">
            <span class="title">Kritik dan saran</span>
            <span class="text">Berikan kritik maupun saran anda untuk hima energi melalui website ini!</span>
        </span>
        <span class="arrow"></span>
    </a>
</div>