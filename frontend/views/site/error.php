<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="divider"></div>

<div class="content">
    <div class="container">
        <div class="site-error">

            <h1><?= Html::encode($this->title) ?></h1>
            <div class="alert alert-danger">
                <?= nl2br(Html::encode($message)) ?>
            </div>
            <p>
                Halaman yang anda inginkan tidak ada. Kemungkinan anda salah memasukkan alamat.
            </p>
            <p>
                Silahkan menghubungi administrator jika kamu mendapatkan pesan error. Terimakasih.
            </p>
        </div>
  
    </div>
</div>

<br>
<br>