<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'HIMA ENERGI PENS';
?>
<div class="slider">
    <ul class="bxslider">
        <li>
            <div class="container">
                <div class="info">
                    <h2> Selamat Datang<br><span>HIMA ENERGI PENS</span></h2>
                    <?php echo Html::a('Checkout more details', ['site/profil']);?>
                </div>
            </div>
            <!-- / content -->
        </li>
        <li>
            <div class="container">
                <div class="info">
                    <h2>Berita <br><span>Kabar Terbaru</span></h2>
                    <?php echo Html::a('Checkout more details', ['berita/index']);?>
                </div>
            </div>
            <!-- / content -->
        </li>
        <li>
            <div class="container">
                <div class="info">
                    <h2>Agenda <br><span>Timeline Himpunan</span></h2>
                    <?php echo Html::a('Checkout more details', ['agenda/index']);?>
                </div>
            </div>
            <!-- / content -->
        </li>
    </ul>
    <div class="bg-bottom"></div>
</div>

<section class="posts">
    <div class="container">
        <article>
            <div class="pic"><img width="121" src="<?php echo Yii::getAlias('@depan');?>/tema/images/2.png" alt=""></div>
            <div class="info">
                <h3>Profil</h3>
                <p>
                    Sebuah wadah yang bersifat organisasi mahasiswa di lingkup program studi Sistem Pembangkitan Energi. Hima Energi merupakan himpunan mahasiswa ke tujuh yang ada dilingkup ormawa PENS dengan memiliki ciri khas warna coklat. <br>
                    <?php echo Html::a('Read more', ['site/profil'], ['class'=>'more']);?>
                </p> 
                     
            </div>
        </article>
        <article>
            <div class="pic"><img width="121" src="<?php echo Yii::getAlias('@depan');?>/tema/images/3.png" alt=""></div>
            <div class="info">
                <h3>Budaya</h3>
                <p>Berani dan kekeluargaan merupakan budaya yang dipegang erat oleh seluruh anggota Hima Energi. Berani mengungkapkan pendapat, ide, gagasa, dll serta bertanggung jawab dengan asas kekeluargaan merupakan implementasi dari budaya SPE.</p>
            </div>
        </article>
    </div>
    <!-- / container -->
</section>

<section class="news">
    <div class="container">
        <h2>BERITA TERBARU</h2>
        
        <article>
            <div class="pic"><img src="<?php echo Yii::getAlias('@depan');?>/tema/images/1.png" alt=""></div>
            <div class="info">
                <h4>Kampanye Debat Suksesi Hima Energi 2017<br/>Jilid II 1</h4>
                <p class="date">21 JUNI 2017, Admin</p>
                <p>Setelah dilaksanakannya Kampanye Debat Suksesi Hima Energi 2017 Jilid I pada tanggal 20 Juni 2017, Kampanye Debat Suksesi Hima Energi Jilid ke 2 dilaksanakan ditempat yang sama dengan mengusung tema "Peran dan Fungsi Ketua Himpunan Mahasiswa Energi PENS di lingkup KM PENS" </p>
                <?php echo Html::a('Read more', ['berita/details'], ['class'=>'more btn']);?>
            </div>
        </article>

        <article>
            <div class="pic"><img src="<?php echo Yii::getAlias('@depan');?>/tema/images/1.png" alt=""></div>
            <div class="info">
                <h4>Kampanye Debat Suksesi Hima Energi 2017<br/>Jilid II 2</h4>
                <p class="date">21 JUNI 2017, Admin</p>
                <p>Setelah dilaksanakannya Kampanye Debat Suksesi Hima Energi 2017 Jilid I pada tanggal 20 Juni 2017, Kampanye Debat Suksesi Hima Energi Jilid ke 2 dilaksanakan ditempat yang sama dengan mengusung tema "Peran dan Fungsi Ketua Himpunan Mahasiswa Energi PENS di lingkup KM PENS" </p>
                <?php echo Html::a('Read more', ['berita/details'], ['class'=>'more btn']);?>
            </div>
        </article>

        <article>
            <div class="pic"><img src="<?php echo Yii::getAlias('@depan');?>/tema/images/1.png" alt=""></div>
            <div class="info">
                <h4>Kampanye Debat Suksesi Hima Energi 2017<br/>Jilid II 3</h4>
                <p class="date">21 JUNI 2017, Admin</p>
                <p>Setelah dilaksanakannya Kampanye Debat Suksesi Hima Energi 2017 Jilid I pada tanggal 20 Juni 2017, Kampanye Debat Suksesi Hima Energi Jilid ke 2 dilaksanakan ditempat yang sama dengan mengusung tema "Peran dan Fungsi Ketua Himpunan Mahasiswa Energi PENS di lingkup KM PENS" </p>
                <?php echo Html::a('Read more', ['berita/details'], ['class'=>'more btn']);?>
            </div>
        </article>

        <article>
            <div class="pic"><img src="<?php echo Yii::getAlias('@depan');?>/tema/images/1.png" alt=""></div>
            <div class="info">
                <h4>Kampanye Debat Suksesi Hima Energi 2017<br/>Jilid II 4</h4>
                <p class="date">21 JUNI 2017, Admin</p>
                <p>Setelah dilaksanakannya Kampanye Debat Suksesi Hima Energi 2017 Jilid I pada tanggal 20 Juni 2017, Kampanye Debat Suksesi Hima Energi Jilid ke 2 dilaksanakan ditempat yang sama dengan mengusung tema "Peran dan Fungsi Ketua Himpunan Mahasiswa Energi PENS di lingkup KM PENS" </p>
                <?php echo Html::a('Read more', ['berita/details'], ['class'=>'more btn']);?>
            </div>
        </article>

        <div class="btn-holder">
            <?php echo Html::a('Read more', ['berita/index'], ['class'=>'blue btn']);?>
        </div>
    </div>
    <!-- / container -->
</section>

<section class="events">
    <div class="container">
        <h2>AGENDA</h2>
        <article>
            <div class="current-date">
                <p>Juni</p>
                <p class="date">02</p>
            </div>
            <div class="info">
                <p>Dies Natalis ke-29 Politeknik Elektronika Negeri Surabaya </p>
                <?php echo Html::a('Read more', ['agenda/details'], ['class'=>'more']);?>
            </div>
        </article>
        <article>
            <div class="current-date">
                <p>Juli</p>
                <p class="date">18</p>
            </div>
            <div class="info">
                <p>Musyawarah Pemilihan Ketua Hima Energi PENS<br/>Periode 2017/2018</p>
                <?php echo Html::a('Read more', ['agenda/details'], ['class'=>'more']);?>
            </div>
        </article>
        <article>
            <div class="current-date">
                <p>Juli</p>
                <p class="date">19</p>
            </div>
            <div class="info">
                <p>Serah Terima Jabatan Badan Pengurus Hima Energi PENS Periode 2016/2017 kepada Badan Pengurus Hima Energi PENS Periode 2017/2018</p>
                <?php echo Html::a('Read more', ['agenda/details'], ['class'=>'more']);?>
            </div>
        </article>
        <article>
            <div class="current-date">
                <p>Desember</p>
                <p class="date">29</p>
            </div>
            <div class="info">
                <p>3rd Anniversary of Hima Energi PENS</p>
                <?php echo Html::a('Read more', ['agenda/details'], ['class'=>'more']);?>
            </div>
        </article>
        <div class="btn-holder">
            <?php echo Html::a('Lihat Semua Agenda', ['agenda/index'], ['class'=>'btn blue']);?>
        </div>
    </div>
    <!-- / container -->
</section>

<div class="container">
    <a href="#fancy" class="info-request">
        <span class="holder">
            <span class="title">Kritik dan saran</span>
            <span class="text">Berikan kritik maupun saran anda untuk hima energi melalui website ini!</span>
        </span>
        <span class="arrow"></span>
    </a>
</div>